package ru.bakirov.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.bakirov.model.Client;

import java.util.List;

@Component
public class ClientDAO {
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public ClientDAO(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Client> index() {
		return jdbcTemplate.query("SELECT * FROM Client",
				new BeanPropertyRowMapper<>(Client.class));
	}

	public Client show(int id) {
		return jdbcTemplate.query("SELECT * FROM Client WHERE id=?",
				new BeanPropertyRowMapper<>(Client.class), id).stream().findAny().orElse(null);
	}

	public void save(Client client) {

		jdbcTemplate.update("INSERT INTO Client " + "(surname, name, patronymic, passport_number, "
		
				+ "family_status, address, phone_number, months_in_current_job, "
				+ "job_position, organization_name) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				client.getSurname(), client.getName(), client.getPatronymic(),
				client.getPassportNumber(), client.getFamilyStatus(), client.getAddress(),
				client.getPhoneNumber(), client.getMonthsInCurrentJob(), client.getJobPosition(),
				client.getOrganizationName());
	}

	public void update(int id, Client updatedClient) {
		jdbcTemplate.update(
				"UPDATE Client SET surname=?, name=?, patronymic=?, passport_number=?, "
						+ "family_status=?, address=?, phone_number=?, months_in_current_job=?, "
						+ "job_position=?, organization_name=? WHERE id=?",
				updatedClient.getSurname(), updatedClient.getName(), updatedClient.getPatronymic(),
				updatedClient.getPassportNumber(), updatedClient.getFamilyStatus(),
				updatedClient.getAddress(), updatedClient.getPhoneNumber(),
				updatedClient.getMonthsInCurrentJob(), updatedClient.getJobPosition(),
				updatedClient.getOrganizationName(), id);
	}

	public void delete(int id) {
		jdbcTemplate.update("DELETE FROM Client WHERE id=?", id);
	}

}
