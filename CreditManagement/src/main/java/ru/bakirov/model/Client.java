package ru.bakirov.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class Client {
	
	private int id;

	@NotEmpty(message = "Surname should not be empty")
	private String surname;

	@NotEmpty(message = "Name should not be empty")
	private String name;
	private String patronymic;

	private String passportNumber;

	@NotEmpty(message = "Family status should not be empty")
	private String familyStatus;

	@NotEmpty(message = "Address should not be empty")
	private String address;

	@Pattern(regexp = "(^$|[+][0-9]{11})")
	private String phoneNumber;
	private int monthsInCurrentJob;
	private String jobPosition;
	private String organizationName;

	public Client() {
	}

	public Client(String surname, String name, String patronymic, String passportNumber,
			String familyStatus, String address, String phoneNumber, int monthsInCurrentJob,
			String jobPosition, String organizationName) {

		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
		this.passportNumber = passportNumber;
		this.familyStatus = familyStatus;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.monthsInCurrentJob = monthsInCurrentJob;
		this.jobPosition = jobPosition;
		this.organizationName = organizationName;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getFamilyStatus() {
		return familyStatus;
	}

	public void setFamilyStatus(String familyStatus) {
		this.familyStatus = familyStatus;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getMonthsInCurrentJob() {
		return monthsInCurrentJob;
	}

	public void setMonthsInCurrentJob(int monthsInCurrentJob) {
		this.monthsInCurrentJob = monthsInCurrentJob;
	}

	public String getJobPosition() {
		return jobPosition;
	}

	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
}
